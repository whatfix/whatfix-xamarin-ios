using Foundation;
using UIKit;

namespace WhatfixSDK
{
	// @interface Whatfix : NSObject
	[BaseType (typeof(NSObject), Name = "_TtC7Whatfix7Whatfix")]
	[DisableDefaultCtor]
	interface Whatfix
	{
		// @property (nonatomic, class) BOOL isDebugEnabled;
		[Static]
		[Export ("isDebugEnabled")]
		bool IsDebugEnabled { get; set; }

		// @property (nonatomic, class) BOOL isLiveMode;
		[Static]
		[Export ("isLiveMode")]
		bool IsLiveMode { get; set; }

		// @property (nonatomic, class) BOOL isShakeEnabled;
		[Static]
		[Export ("isShakeEnabled")]
		bool IsShakeEnabled { get; set; }

		// @property (nonatomic, class) BOOL isMultiTouchEnabled;
		[Static]
		[Export ("isMultiTouchEnabled")]
		bool IsMultiTouchEnabled { get; set; }

		// +(void)initializeWithEntName:(NSString * _Nonnull)entName entId:(NSString * _Nonnull)entId application:(UIApplication * _Nonnull)application;
		[Static]
		[Export ("initializeWithEntName:entId:application:")]
		void InitializeWithEntName (string entName, string entId, UIApplication application);

		// +(void)setEditorMode;
		[Static]
		[Export ("setEditorMode")]
		void SetEditorMode ();

		// +(void)disableEditorMode;
		[Static]
		[Export ("disableEditorMode")]
		void DisableEditorMode ();

		// +(void)setWithLoggedInUserId:(NSString * _Nonnull)loggedInUserId;
		[Static]
		[Export ("setWithLoggedInUserId:")]
		void SetWithLoggedInUserId (string loggedInUserId);

		// +(void)setWithLoggedInUserRole:(NSString * _Nonnull)loggedInUserRole;
		[Static]
		[Export ("setWithLoggedInUserRole:")]
		void SetWithLoggedInUserRole (string loggedInUserRole);

		// +(void)setWithLanguage:(NSString * _Nonnull)language;
		[Static]
		[Export ("setWithLanguage:")]
		void SetWithLanguage (string language);

		// +(void)setWithWhatfixHost:(NSString * _Nonnull)whatfixHost;
		[Static]
		[Export ("setWithWhatfixHost:")]
		void SetWithWhatfixHost (string whatfixHost);

		// +(void)setWithCustomContentLocation:(NSString * _Nonnull)cdnHost;
		[Static]
		[Export ("setWithCustomContentLocation:")]
		void SetWithCustomContentLocation (string cdnHost);

		// +(void)setWithScreenId:(NSString * _Nonnull)screenId;
		[Static]
		[Export ("setWithScreenId:")]
		void SetWithScreenId (string screenId);

		// +(void)setWithIsReact:(BOOL)isReact;
		[Static]
		[Export ("setWithIsReact:")]
		void SetWithIsReact (bool isReact);

		// +(void)setWithIsDrawerOpen:(BOOL)isDrawerOpen;
		[Static]
		[Export ("setWithIsDrawerOpen:")]
		void SetWithIsDrawerOpen (bool isDrawerOpen);

		// +(void)setWithCustomKeyValue:(NSDictionary<NSString *,id> * _Nonnull)customKeyValue;
		[Static]
		[Export ("setWithCustomKeyValue:")]
		void SetWithCustomKeyValue (NSDictionary<NSString, NSObject> customKeyValue);

		// +(void)setTLMarginWithLeft:(NSNumber * _Nonnull)left top:(NSNumber * _Nonnull)top right:(NSNumber * _Nonnull)right bottom:(NSNumber * _Nonnull)bottom;
		[Static]
		[Export ("setTLMarginWithLeft:top:right:bottom:")]
		void SetTLMarginWithLeft (NSNumber left, NSNumber top, NSNumber right, NSNumber bottom);

		// +(void)setWithFeatureFlag:(NSString * _Nonnull)featureFlag isEnabled:(BOOL)isEnabled;
		[Static]
		[Export ("setWithFeatureFlag:isEnabled:")]
		void SetWithFeatureFlag (string featureFlag, bool isEnabled);
	}
}
